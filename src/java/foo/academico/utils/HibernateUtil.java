/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foo.academico.utils;

import foo.academico.model.Aluno;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author Geissiane
 */
public class HibernateUtil {
 
    private static SessionFactory sessionFactory;

    public HibernateUtil() {
    }

    public static SessionFactory getSessionFactory() {

        if (sessionFactory == null) {
            try {
                AnnotationConfiguration db = new AnnotationConfiguration();
                db.addAnnotatedClass(Aluno.class);// MOR(Modelo Objeto Relacional)
                sessionFactory = db.configure().buildSessionFactory();

            } catch (Exception e) {
                System.out.println("Erro ao criar conexão: " + e.getMessage());
            }
            return sessionFactory;
        } else {
            return sessionFactory;
        }

    }

}