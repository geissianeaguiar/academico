/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 package foo.academico.controller;

import foo.academico.dao.CrudAcademico;
import foo.academico.dao.AlunoDao;
import foo.academico.model.Aluno;
import foo.academico.relatorio.Relatorio;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Geissiane
 */
@ManagedBean(name = "alunoController", eager = true)
@SessionScoped
public class AlunoController implements Serializable{
    
    private Aluno aluno;
    private DataModel listarAlunos;
    private CrudAcademico dao;
    
    public AlunoController(){
        this.dao = new AlunoDao();
    }
    
    /**
     * Carrega o formulário para adicionar um novo veiculo
     * contendo todos os campos populados para serem
     */
    public String actionAlterar() {
        aluno = (Aluno) (listarAlunos.getRowData());
        return "manterAluno";
    }
    
    /**
     * Exclui do banco de dados o veiculo selecionado no GRID
     * da dataTable, retornando para a página principal
     */
    public String actionExcluir() {
        Aluno objTemporario = (Aluno) (listarAlunos.getRowData());
        dao.deletar(objTemporario);
        return "index";
    }
    
    public String actionManterAluno(){
        aluno = new Aluno();
        return "manterAluno";
    }
    
    /**
     * Salva um novo veiculo no banco de dados
     */
    public String actionGravarNovo(){
        dao.salvar(aluno);
        return "index";
    }
    /**
     * Salva os dados do veiculo com as alterações 
     * e retorna para a página principal atualizado a dataTable.
     * @return index.html
     */
    public String actionSalvarAlteracao(){
        dao.alterar(aluno);
        return "index";
    }
    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public DataModel getListarAlunos() {
        List<Aluno> lista = new AlunoDao().listar();
        listarAlunos = new ListDataModel(lista);
        return listarAlunos;
    }

    public void setListarAlunos(DataModel listarAlunos) {
        this.listarAlunos = listarAlunos;
    }
    
    public void gerarRelatorioAction() {
        Relatorio relatorio = new Relatorio();
        relatorio.getRelatorio();
    }
    
}