/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package foo.academico.dao;

import foo.academico.model.Aluno;
import foo.academico.utils.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Geissiane
 */
public class AlunoDao implements CrudAcademico<Aluno> {

    @Override
    public void salvar(Aluno obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction operacaoSQL = session.beginTransaction();
        session.save(obj);
        operacaoSQL.commit();
    }

    @Override
    public Aluno getById(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Aluno) session.load(Aluno.class, id);
    }

    @Override
    public List<Aluno> listar() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction operacaoSQL = session.beginTransaction();
        List objetos = session.createQuery("from Aluno").list();
        operacaoSQL.commit();
        return objetos;
    }

    @Override
    public void deletar(Aluno obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction operacaoSQL = session.beginTransaction();
        session.delete(obj);
        operacaoSQL.commit();
    }

    @Override
    public void alterar(Aluno obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction operacaoSQL = session.beginTransaction();
        session.update(obj);
        operacaoSQL.commit();
    }

}
