/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foo.academico.dao;
import java.util.List;

/**
 *
 * @author Geissiane
 */
public interface CrudAcademico<T> {
    
    public void salvar(T obj);

    public T getById(long id);

    public List<T> listar();

    public void deletar(T obj);

    public void alterar(T obj);
}

